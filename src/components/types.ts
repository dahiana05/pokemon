
export type Pokemon = {
    nombre: string,
    imagen: string,
    ataque: number,
    defensa: number,
    id: string
}