import React from "react";
import {
    TableContainer,
    Cell,
    Columns,
    Rows,
    Icon,
    ButtonContainer,
    Imagen
} from "./styled";
import { Pokemon } from "../types";

export const Table = (props : {
    listOfPokemon: Pokemon[];
}) => {
    const editPkmn = (id: string) => {
    }
    const eliminar = (id: string) => {
        const continueDelete = window.confirm("¿Esta seguro que desea eliminar este pokemon?")
        if(continueDelete){
            const requestOptions = {
                method: 'DELETE'
            };
    
            fetch("https://tribu-ti-staffing-desarrollo-afangwbmcrhucqfh.z01.azurefd.net/pkm-msa-evaluation/pokemon/" + id, requestOptions).then(response => response.text()).then(result => {
                alert("Pokemon eliminado con exito")
                window.location.reload()
            }).catch(error => console.log('error', error));
        }

    }
    const listItems = props.listOfPokemon.map((pkmn) => <>
        <Columns>{
            pkmn.nombre
        }</Columns>
        <Columns><Imagen src={
            pkmn.imagen
        }/>
        </Columns><Columns>{
            pkmn.ataque
        }</Columns>
        <Columns>{
            pkmn.defensa
        }</Columns>
        <Columns>
            <ButtonContainer onClick={()=>editPkmn(pkmn.id)}>
                <Icon className="fa fa-pencil"></Icon>
            </ButtonContainer>
            <ButtonContainer onClick={
                        () => eliminar(pkmn.id)
                    }>
                <Icon className="fa fa-trash-o"></Icon>
            </ButtonContainer>
        </Columns>
    </>);
    return (
        <TableContainer>
            <Cell>
                <Rows>Nombre</Rows>
                <Rows>Imagen</Rows>
                <Rows>Ataque</Rows>
                <Rows>Defensa</Rows>
                <Rows>Acciones</Rows>
            </Cell>
            <Cell> {listItems} </Cell>
        </TableContainer>


    )
}

export default Table;
