import {render, screen} from '@testing-library/react'
import '@testing-library/jest-dom'
import Table from './table'
import { Pokemon } from '../types'

test('renders a message', () => {
    const pokemons: Pokemon[] = [
        {
            "nombre": "pikachu",
            "imagen": "https://www.primecomics.com.co/images/feature_variant/3/pikachu.jpg",
            "ataque": 64,
            "defensa": 77,
            "id": "2"
        },
        {
            "nombre": "charizard",
            "imagen": "https://assets.pokemon.com/assets/cms2/img/pokedex/full/006.png",
            "ataque": 11,
            "defensa": 66,
            "id": "4"
        },
        {
            "nombre": "Bulbasor",
            "imagen": "https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png",
            "ataque": 82,
            "defensa": 54,
            "id": "8"
        },
        {
            "nombre": "Alakazam",
            "imagen": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/065.png",
            "ataque": 79,
            "defensa": 14,
            "id": "9"
        },
        {
            "nombre": "Rattata",
            "imagen": "https://assets.pokemon.com/assets/cms2/img/pokedex/full/019.png",
            "ataque": 70,
            "defensa": 20,
            "id": "16"
        },
        {
            "nombre": "Metapod",
            "imagen": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/011.png",
            "ataque": 29,
            "defensa": 81,
            "id": "18"
        },
        {
            "nombre": "Pidgey",
            "imagen": "https://assets.pokemon.com/assets/cms2/img/pokedex/detail/016.png",
            "ataque": 50,
            "defensa": 50,
            "id": "19"
        }
    ]
  render(<Table listOfPokemon={pokemons} />); 
  const title = screen.getByText(/Nombre/);
  expect (title ).toBeInTheDocument();

})