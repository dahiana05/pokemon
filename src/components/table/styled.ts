import styled from 'styled-components'

export const TableContainer = styled.table`
background-color: white;
display: flex;
flex-direction: column;
align-items: flex-start;
margin: 1rem;
border:none;s
`
export const Cell = styled.tr`
color: black;
display: grid;
grid-template-columns: 20% 20% 20% 20% 20%;
min-width: 100%;
`
export const Columns = styled.td`
color: black;
border solid 1px gray;
min-width: 100%;
padding: 0;
height: 40px;
display: flex;
align-items: center;
justify-content: space-evenly;
`
export const Rows = styled.th`
color: black;
border solid 1px gray;
min-width: 100%;
padding: 0;
height: 40px;
display: flex;
align-items: center;
justify-content: space-evenly;
`

export const ButtonContainer = styled.button`
color: #6958eb;
background-color: white;
border: none;
cursor: pointer;
`
export const Icon = styled.i`
`
export const Imagen = styled.img`
height: 40px;
    width: 40px;
`
