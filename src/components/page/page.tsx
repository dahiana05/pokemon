import React, { useEffect, useState } from "react";
import {
    Title,
    Container,
    Input,
    Button,
    Icon,
    IconSearch,
    Row
} from "./styled";

import {PokemonInformation} from '../pokemonInformation/pokemonInformation';
import {Table} from '../table/table'
import { Pokemon } from "../types";

export const Page = () => {
    const [data, setData] = useState<Pokemon[]>([]);

    useEffect(() => {
        renderMyData();
      }, [])
    const renderMyData = () => {
        fetch('https://tribu-ti-staffing-desarrollo-afangwbmcrhucqfh.z01.azurefd.net/pkm-msa-evaluation/pokemon/?idAuthor=1')
            .then((response) => response.json())
            .then((responseJson) => {
                const rsp = responseJson.map((pkmn: any) => { return {nombre: pkmn.name, imagen: pkmn.image, ataque: pkmn.attack, defensa: pkmn.defense, id: pkmn.id}}) 
                setData(rsp)
            })
            .catch((error) => {
              console.error(error);
            });
    }
    return (
        <Container>
            <Title>Listado de Pokemon</Title>
            <Row>
                <Input type="text" placeholder="Buscar"></Input><IconSearch className="fa fa-search"></IconSearch>
                <Button>
                    <Icon className="fa fa-plus"></Icon> Nuevo</Button>
            </Row>
            <Table listOfPokemon={data}></Table>
            <PokemonInformation></PokemonInformation>
        </Container>
    )
}
export default Page