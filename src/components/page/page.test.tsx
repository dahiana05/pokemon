import {render, screen} from '@testing-library/react'
import '@testing-library/jest-dom'
import Page from './page'
import userEvent from '@testing-library/user-event';

test('renders title', () => {
  render(<Page />);
  const title = screen.getByText(/Listado de Pokemon/); 
  expect (title ).toBeInTheDocument();

})

test('validate button is disabled', () => {
  render(<Page />);
  const button = screen.getByRole('button', { name: /Guardar/i });
  expect(button).toBeDisabled();

})

test('validate button is enable after fill inputs', () => {
  render(<Page />);
  const button = screen.getByRole('button', { name: /Guardar/i });
  const nameText = screen.getByPlaceholderText('nombre');
  userEvent.type(nameText, 'pikachu');
  const urlText = screen.getByPlaceholderText('url');
  userEvent.type(urlText, 'https://www.primecomics.com.co/images/feature_variant/3/pikachu.jpg');
  expect(button).toBeEnabled();

})