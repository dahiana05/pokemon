import styled from 'styled-components'

export const Container = styled.div`

`
export const Title = styled.h1`
margin-inline: 1rem;
`
export const Input = styled.input`
font-size: large;
    text-indent: 2rem;
}
`
export const Button = styled.button`
margin: 1rem;
background-color: #6958eb;
border: none;
border-radius: 10px;
color: white;
padding: 15px 32px;
font-size: 16px;
`
export const IconSearch = styled.i`
position: absolute;

    padding-inline: 1rem;
    top: calc(19% - 0.5em);
`
export const Icon = styled.i`
`
export const Row = styled.div`

    display: flex;
    justify-content: space-between;
    margin-inline: 1rem;
`