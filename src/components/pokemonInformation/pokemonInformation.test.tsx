import {render, screen} from '@testing-library/react'
import '@testing-library/jest-dom'
import PokemonInformation from './pokemonInformation'

test('renders a message', () => {
  render(<PokemonInformation />);
  const title = screen.getByText(/Nuevo Pokemon/);
  expect (title ).toBeInTheDocument();

})