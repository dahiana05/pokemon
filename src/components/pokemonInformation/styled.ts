import styled from 'styled-components'

export const Container = styled.div`
border: solid 1px gray;
margin: 1rem;

`
export const ContainerRange = styled.div`
display: grid;
grid-template-columns: 10% 80% 10%;
`
export const Title = styled.h1`
text-align: center;
`
export const Text = styled.p`
text-align: center;
font-size: larger;
`
export const Form = styled.div`
text-align: center;

display: grid;
grid-template-columns: 15% 35% 15% 35%;
margin: 2rem;

`
export const Input = styled.input`
font-size: large;
`
export const Button = styled.button`
margin: 1rem;
background-color: #6958eb;
border: none;
border-radius: 10px;
color: white;
padding: 15px 32px;
font-size: 16px;
cursor: pointer;
`
export const ButtonContainer = styled.div`
display: flex;

justify-content: center;
`
export const Icon = styled.i`
`
