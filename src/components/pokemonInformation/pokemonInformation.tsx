import React, {useEffect, useState} from "react";
import {
    Title,
    Container,
    Text,
    Form,
    Input,
    ContainerRange,
    Button,
    ButtonContainer,
    Icon
} from "./styled";

export const PokemonInformation = () => {
    const [nombre, setNombre] = useState("")
    const [url, setUrl] = useState("")
    const [ataque, setAtaque] = useState(50)
    const [defensa, setDefensa] = useState(50)

    const [isDisabled, setIsDisabled] = useState(true)
    useEffect(() => {
        setIsDisabled(true)
        if (nombre !== "" && url !== "" && ataque !== null && defensa !== null) {
            setIsDisabled(false)
        }
    }, [nombre, url])

    const handleChangeNombre = (e : any) => {
        setNombre(e.target.value)
    }

    const handleChangeUrl = (e : any) => {
        setUrl(e.target.value)
    }

    const handleChangeAtaque = (e : any) => {
        setAtaque(e.target.value)
    }


    const handleChangeDefensa = (e : any) => {
        setDefensa(e.target.value)
    }
    const limpiarDatos = () => {
        setNombre("");
        setUrl("");
        setAtaque(50);
        setDefensa(50);
        window.location.reload();
    }
    const guardar = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        const raw = JSON.stringify({
            "name": nombre,
            "image": url,
            "attack": ataque,
            "defense": defensa,
            "idAuthor": 1
        });

        const requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw
        };

        fetch("https://tribu-ti-staffing-desarrollo-afangwbmcrhucqfh.z01.azurefd.net/pkm-msa-evaluation/pokemon/", requestOptions).then(response => response.text()).then(result => {
            limpiarDatos()
            window.location.reload()

        }).catch(error => console.log('error', error));

    }
    const cancelar = () => {
        limpiarDatos()
    }
    return (
        <Container>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
            <Title>Nuevo Pokemon</Title>
            <Form>
                <Text>Nombre:
                </Text>
                <Input onChange={handleChangeNombre}
                    type="text"
                    placeholder="nombre"></Input>
                <Text>Ataque:
                </Text>
                <ContainerRange>
                    <Text>0
                    </Text>
                    <Input onChange={handleChangeAtaque}
                        type="range"
                        min="0"
                        max="100"
                        step="1"></Input>
                    <Text>100
                    </Text>
                </ContainerRange>
            </Form>

            <Form>
                <Text>Imagen:
                </Text>
                <Input onChange={handleChangeUrl}
                    type="text"
                    placeholder="url"></Input>
                <Text>Defensas:
                </Text>
                <ContainerRange>
                    <Text>0
                    </Text>
                    <Input onChange={handleChangeDefensa}
                        type="range"
                        min="0"
                        max="100"
                        step="1"></Input>
                    <Text>100
                    </Text>

                </ContainerRange>
            </Form>
            <ButtonContainer>
                <Button disabled={isDisabled}
                    onClick={
                        () => guardar()
                    }
                    style={
                        isDisabled ? {
                            opacity: "50%",
                            cursor: "inherit"
                        } : {}
                }>
                    <Icon className="fa fa-floppy-o"></Icon>
                    Guardar</Button>
                <Button onClick={
                    () => cancelar()
                }>
                    <Icon className="fa fa-close"></Icon>
                    Cancelar</Button>
            </ButtonContainer>
        </Container>


    )
}

export default PokemonInformation;