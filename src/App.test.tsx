import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
describe('app-test', () => {
  test('renders Listado de Pokemon', () => {
    render(<App />);
    const title = screen.getByText(/Listado de Pokemon/);
    expect(title).toBeInTheDocument();
  });
})

